# samsung-next-whisk

## Getting Started

These instructions will help you to run project on your machine for testing purposes 

## Built With

* [Maven](https://maven.apache.org/) - Build tool
* [JUnit](https://junit.org/junit5/) - Java testing framework
* [Selenide](https://selenide.org) - WebDriver wrapper framework
* [RestAssured](https://rest-assured.io/) - Testing REST services in Java
 
## Running the tests
```console
$ ./mvn clean test
```
